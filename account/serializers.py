from rest_framework import serializers

from account.models import Account



class RegistrationSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={'input_type':'password'},write_only=True)
    class Meta:
        model=Account
        fields = ['email','username','password','password2']
        extra_kwargs = {
            'password': {'write_only':True}
        }
    def save(self):
        account = Account(
                    email=self.validated_data['email'],
                    username=self.validated_data['username'],
            )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        if password != password2:
            raise serializers.ValidationError({'password':'Passwords do not match'})
        account.set_password(password)
        account.save()
        return account



class UserSigninSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

class UserSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    is_admin = serializers.BooleanField(default=False)
    is_active = serializers.BooleanField(default=True)
    is_staff = serializers.BooleanField(default=False)
    is_superuser = serializers.BooleanField(default=False)
    #password = serializers.CharField(required=True)
