from django.urls import path
from account.views import (
        registration_view,
        signin_view
    )

app_name = "account"

urlpatterns =[
    path('register', registration_view, name="register"),
    path('login', signin_view, name="login"),
]